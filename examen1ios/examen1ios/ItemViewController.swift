//
//  ItemViewController.swift
//  examen1ios
//
//  Created by Juan Sebastian Benavides Cardenas on 11/12/17.
//  Copyright © 2017 Juan Sebastian Benavides Cardenas. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import AlamofireObjectMapper

class ItemViewController: UIViewController {

   
    @IBOutlet weak var txtItem: UITextField!
    @IBOutlet weak var lblCaegoria: UILabel!
    @IBOutlet weak var lblCosto: UILabel!
    
    @IBOutlet weak var ImgItem: UIImageView!
    @IBOutlet weak var lblNombre: UILabel!
    
    @IBOutlet weak var lblAtributo: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func buscarItem(_ sender: Any)
    {
        var image:String?
        Alamofire.request("https://pokeapi.co/api/v2/item/"+txtItem.text!).responseObject
            { (response: DataResponse<Items>)
                in
                
                let item = response.result.value
                
                
                DispatchQueue.main.async
                    {
                        self.lblNombre.text = "\(item?.itName ?? "N/D")"
                        self.lblCaegoria.text = "\(item?.itCategoria ?? "N/D")"
                        self.lblCosto.text = "\(item?.itCost ?? 0)"
                        self.lblAtributo.text =
                            item?.itAtributte ?? "N/D"
                        image = item?.itImg ?? ""
                        Alamofire.request(image!).responseImage
                            {
                                response
                                in
                                guard let image = response.result.value
                                    else
                                {
                                    return
                                }
                                self.ImgItem.image = image
                                
                                
                        }
    }
    }

}
}
