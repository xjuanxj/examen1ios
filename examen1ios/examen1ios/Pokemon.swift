//
//  Pokemon.swift
//  examen1ios
//
//  Created by Juan Sebastian Benavides Cardenas on 5/12/17.
//  Copyright © 2017 Juan Sebastian Benavides Cardenas. All rights reserved.
//


import Foundation
import ObjectMapper
class Pokemon:Mappable
{
    var pkExp:Double?
    var pkWeight:Double?
    var pkheight:Double?
    var pkabilities:[NSDictionary]?
    var pkabilidad:String?
    var pkimg:String?
    var pkName:String?

    
    
    required init?(map: Map){
        
    }
    func mapping(map: Map) {
        pkName <- map["name"]
        pkExp <- map["base_experience"]
        pkWeight <- map["weight"]
        pkheight <- map["height"]
        pkabilities <- map["abilities"]
        pkimg <- map ["sprites.front_default"]
        pkabilidad=""
        if pkabilities != nil
        {
        for w in pkabilities!
        {
          let dicionario = w as! NSDictionary
          let mini = dicionario["ability"]  as! NSDictionary
          let  pkabilidad1 = mini["name"] as! String
          pkabilidad! += pkabilidad1 + "\n"
            
            
        }
         print(pkabilidad!)
        }
        else
        {
            pkabilidad="N/D"
        }
        
    }
}
