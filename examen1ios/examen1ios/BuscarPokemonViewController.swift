//
//  BuscarPokemonViewController.swift
//  examen1ios
//
//  Created by Juan Sebastian Benavides Cardenas on 5/12/17.
//  Copyright © 2017 Juan Sebastian Benavides Cardenas. All rights reserved.
//

import UIKit
import Alamofire //importar alamofire
import AlamofireObjectMapper
import AlamofireImage
class BuscarPokemonViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var labelPeso: UILabel!
    @IBOutlet weak var labelAltura: UILabel!
    @IBOutlet weak var labelExp: UILabel!
    
    @IBOutlet weak var lblNOmbre: UILabel!
    @IBOutlet weak var pokemon: UIImageView!
    @IBOutlet weak var txtPokemon: UITextField!
    @IBOutlet weak var txthabilidad: UITextView!
    
    @IBAction func buttonConsultar(_ sender: Any)
    {
        var image:String?
        Alamofire.request("https://pokeapi.co/api/v2/pokemon/"+txtPokemon.text!).responseObject
            { (response: DataResponse<Pokemon>)
                in
                
                let pokemon = response.result.value
                
                
                DispatchQueue.main.async
                    {
                        self.lblNOmbre.text = pokemon?.pkName
                        self.labelExp.text = "\(pokemon?.pkExp ?? 0)"
                        self.labelAltura.text = "\(pokemon?.pkheight ?? 0)"
                        self.labelPeso.text = "\(pokemon?.pkWeight ?? 0)"
                       self.txthabilidad.text =
                        pokemon?.pkabilidad ?? "N/D"
                        image = pokemon?.pkimg ?? ""
                        Alamofire.request(image!).responseImage
                            {
                                response
                                in
                                guard let image = response.result.value
                                    else
                                {
                                    return
                                }
                                self.pokemon.image = image
                        
                        
                }
                
    }
    
}
}
}
