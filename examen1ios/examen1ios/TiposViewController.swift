//
//  TiposViewController.swift
//  examen1ios
//
//  Created by Juan Sebastian Benavides Cardenas on 11/12/17.
//  Copyright © 2017 Juan Sebastian Benavides Cardenas. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import AlamofireObjectMapper

class TiposViewController: UIViewController {

    @IBOutlet weak var txtTipo: UITextField!
    @IBOutlet weak var lblNDF: UITextView!
    @IBOutlet weak var MDF: UITextView!
    @IBOutlet weak var DDF: UITextView!
    @IBOutlet weak var NDD: UITextView!
    @IBOutlet weak var MDD: UITextView!
    @IBOutlet weak var DDD: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var buscarTIpo: UIButton!
    
    @IBAction func buscarTipo(_ sender: Any)
    {
        Alamofire.request("https://pokeapi.co/api/v2/type/" + txtTipo.text! + "/").responseObject
            { (response: DataResponse<Tipo>)
                in
                
                let tipo = response.result.value
                
                
                DispatchQueue.main.async
                    {
                       self.lblNDF.text = tipo?.pknoDañoDe
                        self.MDF.text = tipo?.pkmedioDañoDe
                        self.DDF.text = tipo?.pkdobleDañoDe
                        self.NDD.text = tipo?.pknoDañoPara
                        self.MDD.text = tipo?.pkmedioDañoPara
                        self.DDD.text = tipo?.pkdobleDañoPara
                        
                      
                }
                        
        
    }
    
    
}
}

