# README #

Esta es una aplicaci�n creada por Juan Benavides para el examen del primer bimestre de la asignatura de IOS  
La aplicaci�n se creo usando la API de pokemon con ayuda de esta se pudo buscar pokemones por id de pokedex y por nombre y recuperar un sprite con datos b�sicos de los pokemons, adem�s se impelemto una pantalla de tabla de tipos que busca el\n
tipo de pokemon y muestra los da�os dobles, da�os medios, no da�os. Finalmente la pantalla items muestra el sprite nombre y costo  del item a buscar.


La aplicaci�n usa PODs especificacmente:  
		#Alamofire  
		#AlamofireMapper  
		#AlamofireImage  
----------------------------------------------------------------------------------------  
# Configuraci�n podfile  #
\# Uncomment the next line to define a global platform for your project  
\# platform :ios, '9.0'  

target 'examen1ios' do  
  \# Comment the next line if you're not using Swift and don't want to use dynamic frameworks  
  use_frameworks!  

  \# Pods for examen1ios  
   pod 'Alamofire', '~> 4.5'  
   pod 'AlamofireObjectMapper', '~> 5.0'  
   pod 'AlamofireImage', '~> 3.3'  
end
